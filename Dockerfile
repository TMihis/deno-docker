FROM --platform=linux/amd64 alpine:3.11.6

RUN apk add --no-cache bash curl \
    && curl -fsSL https://deno.land/x/install/install.sh | sh \
    && export DENO_INSTALL="/root/.deno" \
    && export PATH="$DENO_INSTALL/bin:$PATH"

